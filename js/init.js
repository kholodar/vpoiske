jQuery.fn.ForceNumericOnly = function() {
    return this.each(function() {
        $(this).keydown(function(e) {
            var key = e.charCode || e.keyCode || 0;
            return (key == 8 || key == 9 || key == 46 || (key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
        });
    });
};

$(function () { 
    Highcharts.setOptions({
        lang:{
            contextButtonTitle: 'Контекстное меню графика',
            loading: 'Загрузка...',
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
            exportButtonTitle: "Экспорт",
            printButtonTitle: "Печать",
            rangeSelectorFrom: "С",
            rangeSelectorTo: "По",
            rangeSelectorZoom: "Период",
            downloadPNG: 'Скачать PNG',
            downloadJPEG: 'Скачать JPEG',
            downloadPDF: 'Скачать PDF',
            downloadSVG: 'Скачать SVG',
            printChart: 'Напечатать график',
            noData: 'Нет данных для отображения',
            resetZoom: 'Отменить увеличение',
            decimalPoint: ',',
            thousandsSep: ' '
        },
        xAxis:{
            dateTimeLabelFormats:{
            millisecond: '%H:%M:%S.%L',
            second: '%H:%M:%S',
            minute: '%H:%M',
            hour: '%H:%M',
            day: '%e %b',
            week: '%e.%b.%y' ,
            month: '%b \'%y',
            year: '%Y'
            }
        }
    });
    
    $('#container1').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Прогноз стоимости 2-к. квартир в Херсонe (доллар США/м2)'
        },
        xAxis: {
            type: 'datetime',
            title: {
                text: 'Дата'
            }
        },
        yAxis: {
            title: {
                text: ''
            },
            min: 0,
             lineWidth: 1
        },
       
        tooltip: {
            shared: true,
            useHTML: true,
             headerFormat: '<small> </small><table>',
            pointFormat: '<tr><td style="text-align: right; color:white">${point.y}</td></tr>',
            footerFormat: '</table>',
            valueDecimals: 0,
             backgroundColor: '#556270',
            borderColor: '#556270'
        },
         plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#429ddd',
                lineWidth: 3,
                marker: {
                    lineWidth: 3,
                    lineColor: '#fff'
                }
            }
        },
        series: [{
            name: 'Прогноз стоимости',
            color:'#9dd4ca',
            // Define the data points. All series have a dummy year
            // of 1970/71 in order to be compared on the same x axis. Note
            // that in JavaScript, months start at 0 for January, 1 for February etc.
            data: [
                [Date.UTC(2014,  0, 1), 200  ],
                [Date.UTC(2014, 0, 2), 300 ],
                [Date.UTC(2014, 0, 3), 320 ],
                [Date.UTC(2014, 0,  4), 220 ],
                [Date.UTC(2014, 0,  5), 260 ],
                [Date.UTC(2014, 0, 6), 600 ],
                [Date.UTC(2014, 0, 7), 800]
            ]
        }]
    });
}); 

$(document).ready(function() {

    // detect transit support
    var transitFlag = Modernizr.cssanimations;

    localStorage.clear(); //не забыть удалить

    function validation() {
        $('.wForm').each(function() {
            var formValid = $(this);
            formValid.validate({
                showErrors: function(errorMap, errorList) {
                    if (errorList.length) {
                        var s = errorList.shift();
                        var n = [];
                        n.push(s);
                        this.errorList = n;
                    }
                    this.defaultShowErrors();
                },
                invalidHandler: function(form, validator) {
                    $(validator.errorList[0].element).trigger('focus');
                    formValid.addClass('no_valid');
                },
                submitHandler: function(form) {
                    formValid.removeClass('no_valid');
                    if (form.tagName === 'FORM') {
                        form.submit();
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: $(form).attr('data-form-url'), /* path/to/file.php */                        
                            data: $(form).find('select, textarea, input').serializeArray(),
                            dataType: 'json',
                            success: function(data) {

                            },
                            error: function() {
                                $.magnificPopup.open({
                                    items: {
                                        src: '<div class="mfiModal" style="max-width: 300px;"><p>Форма отправлена</p><p>Для верстальщика все ок :)</p></div>',
                                        type: 'inline'
                                    },
                                    removalDelay: 450,
                                    mainClass: 'zoom-in'
                                });
                            }
                        });
                    }
                }
            });
        });

        $('.wForm').on('change', '.wFile', function(event) {
            var m = $(this).prop('multiple'), f = this.files,
                label = $(this).siblings('.wFileVal'), t = label.data('txt');
            if (f.length) {
                if (m) {
                    var v = t[1].replace('%num%', f.length), a = [];
                    for (var i = 0; i < f.length; i++) {
                        a.push(f[i].name);
                    }
                    label.html('<span>' + v + ' <ins>('+a.join(', ') + ')</ins></span>'); $(this).blur();
                } else {
                    label.html(t[1]+': '+f[0].name); $(this).blur();
                }
            } else {
                label.html(t[0]);
            }
        })

        /* Без тега FORM */
        $('.wForm').on('click', '.wSubmit', function(event) {
            var form = $(this).closest('.wForm');
            form.valid();
            if (form.valid()) {
                form.submit();
            }
        });

        /* Сброс Без тега FORM */
        $('.wForm').on('click', '.wReset', function(event) {
            var form = $(this).closest('.wForm');
            if (form.is('DIV')) {
                form.validReset();
            }
        });
    }

    validation();

    /* magnificPopup */

    $('body').magnificPopup({
        delegate: '.mfiA',
        callbacks: {
            elementParse: function(item) {
                mfiID = item.el.data('param').id;
                this.st.ajax.settings = {
                    url: item.el.data('url'),
                    type: 'POST',
                    data: item.el.data('param')
                };
            },
            ajaxContentAdded: function(el) {
                validation();
                if($('.initMasks').length){
                    $('.initMasks').inputmask({
                        "placeholder":"+38__________",
                        "mask": "+389999999999"
                    });
                }
                if($('.stylerSelect').length){
                    $('.stylerSelect').select2({
                        minimumResultsForSearch:-1,
                        placeholder:"Выберите пункт"
                    }).on('change', function(event) {
                        $(this).valid();
                    });
                }    


            }
        },
        type: 'ajax',
        removalDelay: 300,
        mainClass: 'zoom-in'
    });

    /*wTxt iframe*/
    function wTxtIFRAME() {
        var list = $('.wTxt').find('iframe');
        if (list.length) {
            for (var i = 0; i < list.length; i++) {
                var ifr = list[i];
                var filter = /youtu.be|youtube|vimeo/g; // d
                //if (typeof $(ifr).data('wraped') === 'undefined') { // без фильтра
                if (typeof $(ifr).data('wraped') === 'undefined' && !!ifr.src.match(filter)) {
                    var ratio = (+ifr.height / +ifr.width * 100).toFixed(0);
                    $(ifr).data('wraped', true).wrap('<div class="iframeHolder ratio_'+ratio.slice()+'"></div>');
                }
            }
        }
    }

    $('.stylerSelect').select2({
        minimumResultsForSearch:-1,
        placeholder:"Выберите пункт"
    }).on('change', function(event) {
        $(this).valid();
    });

        //Dropzone
        function preview() {
            // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
            var previewNode = document.querySelector("#template");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);
            var maxFiles = $('div#dz').attr('data-max');

            var myDropzone = new Dropzone('div#dz', { // Make the whole body a dropzone
                url: "/wezom/ajax/images/upload_images", // Set the url
                autoProcessQueue: true,
                thumbnailWidth: 120,
                thumbnailHeight: 93,
                maxFiles:maxFiles,
                maxFilesize:1,
                acceptedFiles: ".jpg, .png, .gif",
                parallelUploads: 1,
                previewTemplate: previewTemplate,
                dictRemoveFile: "X",
                addRemoveLinks: true,
                previewsContainer: "#previews", // Define the container to display the previews
                clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
            });

            myDropzone.on("maxfilesexceeded", function(file) {
                this.removeFile(file);
            });

            // Update the total progress bar
            myDropzone.on("totaluploadprogress", function(progress) {
                document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
            });

            myDropzone.on("sending", function(file) {
                // Show the total progress bar when upload starts
                document.querySelector("#total-progress").style.opacity = "1";
                // And disable the start button
                //file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
            });

            // Hide the total progress bar when nothing's uploading anymore
            myDropzone.on("queuecomplete", function(progress) {
                document.querySelector("#total-progress").style.opacity = "0";
            });

            myDropzone.on("maxfilesexceeded", function(file) {
                this.removeFile(file);
            });

            // Setup the buttons for all transfers
            // The "add files" button doesn't need to be setup because the config
            // `clickable` has already been specified.
        }

        if ($('#template').length) {
            preview();
        }

        function preview2() {
            // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
            var previewNode = document.querySelector("#template2");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzone = new Dropzone('div#dz2', { // Make the whole body a dropzone
                url: "/target-url", // Set the url
                autoProcessQueue: false,
                thumbnailWidth: 120,
                thumbnailHeight: 93,
                maxFiles:3,
                maxFilesize:150,
                acceptedFiles: ".mpg, .mpeg, .avi, .wmv, .mov",
                parallelUploads: 1,
                previewTemplate: previewTemplate,
                previewsContainer: '#previews2', // Define the container to display the previews
                clickable: ".fileinput-button2",
            });

            myDropzone.on("maxfilesexceeded", function(file) {
                this.removeFile(file);
            });

            // Update the total progress bar
            myDropzone.on("totaluploadprogress", function(progress) {
                document.querySelector("#total-progress2 .progress-bar").style.width = progress + "%";
            });

            myDropzone.on("sending", function(file) {
                // Show the total progress bar when upload starts
                document.querySelector("#total-progress2").style.opacity = "1";
                // And disable the start button
                file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
            });

            // Hide the total progress bar when nothing's uploading anymore
            myDropzone.on("queuecomplete", function(progress) {
                document.querySelector("#total-progress2").style.opacity = "0";
            });
        }

        if ($('#template2').length) {
            preview2();
        }

        $('.plusFavorites').on('click', function(event){
            event.preventDefault();
            $(this).toggleClass('lightHeart');
        });

    $('.date_input').pickadate({
        format: 'dd.mm.yyyy',
        today: '',
        clear: '',
        close: ''
    });

    $('.arCol').on('click', function(){
        $('.arCol').toggleClass('click').toggleClass('cur');
    });

    $(window).load(function() {
        if ($('#templateFirst').length) {
            dropFirst();
        }
        wTxtIFRAME();
        $('.spinnsS').styler();
        $('.sliderInitialize').caroufredsel({});
            $("#foo1").carouFredSel({
        responsive:false,
        height:'270',
        items: 1,
        scroll: {
            fx: "crossfade"
        },
        auto: false,
        pagination: {
            container: "#foo1_pag",
            anchorBuilder: function( nr ) {
                var src = $("img", this).attr( "src" );
                    src = src.replace( "/large/", "/small/");
                return '<img src="' + src + '" />';
            }
        }
    });

        $('#thumbs .thumb a').each(function(i) {
                $(this).addClass( 'itm'+i );
                $(this).click(function() {
                        $('#images').trigger( 'slideTo', [i, 0, true] );
                        return false;
                    });
                });
                $('#thumbs a.itm0').addClass( 'selected' );             
                $('#images').carouFredSel({
                    direction: 'left',
                    circular: true,
                    infinite: false,
                    items: 1,
                    auto: false,
                    prev: '#prev2',
                    next: '#next2',
                    onCreate:function(data){
                        data.items.addClass('cur')  
                        },
                    scroll: {
                        fx: 'directscroll',
                        onBefore: function(data) {
                            data.items.visible.addClass('cur')
                            var pos = $(this).triggerHandler( 'currentPosition' );                          
                            $('#thumbs a').removeClass( 'selected' );
                            $('#thumbs a.itm'+pos).addClass( 'selected' );
                            $('#thumbs').trigger( 'slideTo', pos );
                        }
                    }
                });
                $('#thumbs').carouFredSel({
                    items: 4,
                    align: false,
                    auto: false,
                    scroll:1
                });

        $('.carous_wrap').each(function(){
            var th = $(this);
            var next = $('.nextC', th);
            var prev = $('.prevC', th);
            $('.foo2', th).carouFredSel({
                auto: false,
                prev: prev,
                next: next,
                items:5,
                scroll:{
                    items:1,
                    easing:'quadratic'                
                    }               
            });
        });

        $('.datepicker').pickadate();

    });

    function wTab(t) {  
        t.parent().children('.curr').removeClass('curr');
        t.addClass('curr');
        $('.' + t.attr('data-tab-container')).children('.curr').removeClass('curr');
        $('.' + t.attr('data-tab-container')).children('.' + t.attr('data-tab-link')).addClass('curr');
    }

    $('.wTab_Nav').on('click', '.wTab_link', function(event) {
        if ($(this).hasClass('curr')) {
            return false;
        } else {
            wTab($(this));
        }
    });

    $('.numeric').ForceNumericOnly();

    $('.js-show-info').on('click', function(event) {
        event.preventDefault();

        var link = $(this);

        var el = $(this).attr('href');

        $(el).slideToggle('400', function() {
            $(this).toggleClass('hidden-info--show');

            if($(this).hasClass('hidden-info--show')) {
                if(link.find('span').length){
                    link.find('span').text(link.attr('data-show'));
                } else {
                    link.text(link.attr('data-show'));
                }
            } else {
                if(link.find('span').length){
                    link.find('span').text(link.attr('data-hide'));
                } else {
                    link.text(link.attr('data-hide'));
                }
            }
        });
    });

    /*-------------------------------  EnterPopup  ------------------------------------*/

    $('body').on('click', '#forget_pass', function(event) {
        $('#entrForm').removeClass('visForm');
        $('#forgetForm').addClass('visForm');
    });

    $('body').on('click', '#remember_pass', function(event) {
        $('#forgetForm').removeClass('visForm');
        $('#entrForm').addClass('visForm');
    });

    /*-------------------------------  responsive EnterPopup  ------------------------------------*/

    $('body').on('click', '#enterReg .erTitle', function(event) {
        event.preventDefault();
        if($(window).width() < 720) {
            if(!$(this).parent().hasClass('wCur')) {
                $('#enterReg .popupBlock').removeClass('wCur').filter($(this).parent()).addClass('wCur');
            }
        }
    });

    $('.offering-item_like-link').on('click', function(event) {
        event.preventDefault();
        
        $(this).toggleClass('offering-item_like-link--enable');
    });

    $('.unmaskedNumber').on('click', function(e){
        e.preventDefault();
        $(this).remove();
        var phone = $('.telMasked span').attr('data-phone');
        $('.telMasked span').text(phone);
    });

    if($('.profile-body_sideright').length){
        (function() {
            var h = $('.profile-body_sideright').parent().height();
            $('.profile-body_sideright').css('minHeight', h+25);
        })();
    }

    $('.file-styler').styler({
        fileBrowse: 'Загрузить фото'
    });

    $('.js-show-seo').on('click', function(event) {
        event.preventDefault();
        
        $('.seoTxt').toggleClass('seoTxtVisible');

        $(this).remove();
    });

    // Смена города ->

    $("#header-location_change").select2({
        placeholder: "Выбрать другой город"
    });

    $('.select2-results').niceScroll();

    $("#header-location_change").on("select2-selecting", function(e) {
        $('#s2id_header-location_change').css('display', 'none');

        var data = [
                        {id:'Город 1',text:'Город 1'},
                        {id:'Город 2',text:'Город 2'},
                        {id:'Город 3',text:'Город 3'},
                        {id:'Город 4',text:'Город 4'},
                        {id:'Город 5',text:'Город 5'},
                        {id:'Город 6',text:'Город 6'},
                        {id:'Город 7',text:'Город 7'},
                        {id:'Город 8',text:'Город 8'},
                        {id:'Город 9',text:'Город 9'},
                        {id:'Город 10',text:'Город 10'},
                        {id:'Город 11',text:'Город 11'},
                        {id:'Город 12',text:'Город 12'}
                    ];

        $("#header-location_change-city").select2({
            placeholder: "Выбрать другой город",
            data: data
        });

        $("#header-location_change-city").select2("open");

        $('.select2-results').niceScroll();
    });

    $("#header-location_change-city").on("select2-selecting", function(e) { 
        $('#s2id_header-location_change-city').css('display', 'none');
        $('#s2id_header-location_change').css('display', 'block');

        $('#header-location_change').select2("val", "");
    });

    $("#header-location_change-city").on("select2-close", function(e) {
        var city = $("#header-location_change-city").select2("val");
        $('.header-location_curent').text(city);

        $("#header-location_change-city").select2("val", "");
    });

    // Смена города <-



   // Клонирование поля в форме ->
   

   function officeCount() {
        var numberOffice = 1;

        $('.numberOffice').each(function(index, el) {
            $(el).text(numberOffice);
            numberOffice++;
        });
   }

   $('.js-form-clone').on('click', function(event) {
       event.preventDefault();

       var name = new Date().getTime();

       var wFormClone = $($(this).attr('href')).clone(true, false).attr('id', '');

       var inputs = wFormClone.find('input');

       wFormClone.find('.wFormInput label.error').remove();

       if(inputs.length == 1){
           var newName = inputs.attr('name') + '-' + name;
           inputs.attr('name', newName).removeClass('error').val('');
       } else if(inputs.length > 1) {
           inputs.each(function(index, el) {
               var newName = $(el).attr('name') + '-' + name;
               $(el).attr('name', newName).removeClass('error').val('');
           });
       }

       var rowWithSelect = wFormClone.find('select');

       rowWithSelect.each(function(index, el) {
           $(el).closest('.wFormInput').find('.select2-container').remove();
       });

       var selects = wFormClone.find('select');

       if(selects.length == 1){
           var newName = selects.attr('name') + '-' + name;
           selects.attr('name', newName);

           selects.select2({
               minimumResultsForSearch:-1,
               placeholder:"Выберите пункт"
           }).on('change', function(event) {
               $(this).valid();
           });
       } else if(selects.length > 1) {
           selects.each(function(index, el) {
               var newName = $(el).attr('name') + '-' + name;
               $(el).attr('name', newName);

               $(el).select2({
                   minimumResultsForSearch:-1,
                   placeholder:"Выберите пункт"
               }).on('change', function(event) {
                   $(this).valid();
               });
           });
       }

       wFormClone.prepend('<a href="#" class="wFormCloneRemove">Удалить</a>');

       wFormClone.insertBefore($(this));

       officeCount();
   });

   $(document).on('click', '.wFormCloneRemove', function(event) {
       event.preventDefault();
       
       $(this).closest('.wFormClone').remove();

       officeCount();
   });

   $('.timePick').pickatime({
       format: 'H:i',
       formatLabel: 'H:i'
   });

   // Клонирование поля в форме <-

    if($('.seoTxt').height() < 300) {
        $('.js-show-seo').css('display', 'none');
    }

});