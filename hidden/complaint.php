﻿
<div class="formPopup zoomAnim">
    <div class="h2T popupTitle">Оставить жалобу</div>

    <div class="wForm wFormDef" data-form="true">
    
        <div class="wFormRow">
            <div class="popupLabel">Имя* :</div>

            <div class="wFormInput">
                <input type="text" class="wInput" name="name" required data-rule-word="true" data-rule-minlength="3">
            </div>
        </div>
        
        <div class="wFormRow">
            <div class="profile-edit_label">Email* :</div>

            <div class="wFormInput">
                <input type="email" class="wInput" name="email" required data-rule-email="true" data-msg-required="Укажите e-mail" >
            </div>
        </div>

        <div class="wFormRow">
            <div class="wFormInput">
                <select class="wSelect stylerSelect popSel" name="complaint" required data-msg-required="Введите пункт" placeholder="Выберите причину жалобы">
                    <option></option>
                    <option value="v1">причина 1</option>
                    <option value="v2">причина 2</option>
                    <option value="v3">причина 3</option>
                    <option value="v4">причина 4</option>
                </select>
            </div>
        </div>

        <div class="wFormRow">
            <div class="popupLabel">Сообщение* :</div>

            <div class="wFormInput">
                <textarea class="wTextarea" name="msg" required data-rule-minlength="10" data-msg-required="Введите текст" placeholder="Введите ваше сообщение"></textarea>
            </div>
        </div>

        <div class="tac">
            <button class="wSubmit wBtn lg">отправить жалобу</button>
        </div>
    </div>
</div>