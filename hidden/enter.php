﻿<div id="enterReg" class="enterRegPopup zoomAnim">
    <div class="enterReg_top">
        <div class="popupBlock enterBlock wCur">
            <div class="erTitle">Вход на сайт</div>
            <div class="popupContent">
                <div id="entrForm" data-form="true" class="wForm wFormDef enterBlock_form visForm">
                    <div class="wFormRow">
                        <input type="email" name="enter_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail" required="">
                    </div>
                    <!-- .wFormRow -->
                    <div class="wFormRow">
                        <input type="password" name="enter_pass" data-msg-required="Это поле необходимо заполнить" data-msg-minlength="Пожалуйста, введите не меньше 4 символов" data-rule-minlength="4" placeholder="Пароль" required="">
                    </div>
                    <!-- .wFormRow -->
 
                    <label class="wCheck checkBlock">
                        <input type="checkbox" name="save"> 
                        <span>Запомнить данные</span>
                    </label>

                    <div class="passLink" id="forget_pass">Забыли пароль?</div>

                    <!-- .checkBlock -->
                    <div class="tar">
                        <button class="wSubmit wBtn lg">войти</button>
                    </div>
                </div>

                <div id="forgetForm" data-form="true" class="wForm enterBlock_form">
                    <div class="wFormRow">
                        <input type="email" name="forget_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail" required="">
                    </div>
                    <!-- .wFormRow -->
                    <div class="forgetInf">
                        После отправления, в течении 5 минут к Вам на почту придут инструкции по восстановлению пароля.
                    </div>
                    <!-- .forgetInf -->
                    <div class="passLink" id="remember_pass">Вернуться</div>
                    <div class="tar">
                        <button class="wSubmit wBtn lg">отправить</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- .enterBlock -->
        <div data-form="true" class="popupBlock wForm wFormDef regBlock">
            <div class="erTitle">Новый пользователь</div>
            <div class="popupContent">
                <div class="wFormRow">
                    <input type="email" name="reg_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail" required="">
                </div>
                <!-- .wFormRow -->
                <div class="wFormRow">
                    <input type="password" name="reg_pass" data-msg-required="Это поле необходимо заполнить" data-msg-minlength="Пожалуйста, введите не меньше 4 символов" data-rule-minlength="4" placeholder="Пароль" required="">
                </div>
                <!-- .wFormRow -->

                <label class="wCheck checkBlock">
                    <input type="checkbox" name="reg_agree" required data-msg="Данное поле необходимо отметить"> 
                    <span>Я согласен с условиями использования и обработку моих персональных данных</span>
                </label>

                <!-- .checkBlock -->
                <div class="tar">
                    <button class="wSubmit wBtn lg">зарегистрироваться</button>
                </div>
            </div>
            <!-- .popupContent -->
        </div>
        <!-- .regBlock -->
    </div>
    <!-- .enterReg_top -->
    <div class="popupBlock socEnter">
        <div class="erTitle">Вход через соц. сети</div>
        <div class="popupContent socLinkEnter">
            <button class="eVk wBtn lg" title="Вконтакте"></button>
            <button class="eFb wBtn lg" title="Facebook"></button>
            <button class="eOd wBtn lg" title="Одноклассники"></button>
            <button class="eMr wBtn lg" title="Mail.ru"></button>
        </div>
        <!-- .socLinkEnter -->
        <div class="clear"></div>
    </div>
    <!-- .socEnter -->
</div>
<!-- #enterReg -->