﻿
<div class="formPopup zoomAnim">
    <div class="h2T popupTitle">Отправить заявку</div>

    <div class="wForm wFormDef" data-form="true">

        <div class="wFormRow">
            <div class="popupLabel">Имя* :</div>

            <div class="wFormInput">
                <input type="text" class="wInput" name="name" required data-rule-word="true" data-rule-minlength="2">
            </div>
        </div>

        <div class="wFormRow">
            <div class="popupLabel">Телефон* :</div>

            <div class="wFormInput">
                <input type="tel" class="wInput phoneMask initMasks" name="phone" required data-rule-phoneUA="true" data-msg-required="Укажите номер телефона" >
            </div>
        </div>

        <div class="tac">
            <button class="wSubmit wBtn lg">отправить заявку</button>
        </div>
    </div>
</div>