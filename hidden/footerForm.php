<div class="formPopup zoomAnim">
    <div class="h2T popupTitle">Отправить сообщение</div>

    <div class="wForm wFormDef" data-form="true">

        <div class="wFormRow">
            <div class="popupLabel">ФИО :</div>

            <div class="wFormInput">
                <input type="text" class="wInput" name="name" required data-rule-word="true" data-rule-minlength="2">
            </div>
        </div>

        <div class="wFormRow">
            <div class="profile-edit_label">Email* :</div>

            <div class="wFormInput">
                <input type="email" class="wInput" name="email" required data-rule-email="true" data-msg-required="Укажите e-mail" >
            </div>
        </div>

        <div class="wFormRow">

            <div class="popupLabel">Сообщение* :</div>

            <div class="wFormInput">
                <textarea class="wTextarea" id="textarea1" name="textarea1" required data-rule-minlength="10" data-msg-required="Введите текст" placeholder="Введите ваше сообщение"></textarea>
            </div>
        </div>


        <div class="tac">
            <button class="wSubmit wBtn lg">отправить сообщение</button>
        </div>
    </div>
</div>