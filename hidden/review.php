﻿
<div class="formPopup zoomAnim">
    <div class="h2T popupTitle">Добавить отзыв</div>

    <div class="wForm wFormDef" data-form="true">

        <div class="wFormRow">
            <div class="popupLabel">Имя* :</div>

            <div class="wFormInput">
                <input type="text" class="wInput" name="name" required data-rule-word="true" data-rule-minlength="2">
            </div>
        </div>
        
        <div class="wFormRow">
            <div class="profile-edit_label">Email* :</div>

            <div class="wFormInput">
                <input type="email" class="wInput" name="email" required data-rule-email="true" data-msg-required="Укажите e-mail" >
            </div>
        </div>

        <div class="wFormRow">
            <div class="starWrapp revStar">
                <p class="starListLabel">Оставьте оценку*</p>

                <div class="starList">
                    <input type="radio" name="star" value="v5" required><span>5</span>
                    <input type="radio" name="star" value="v4" required><span>4</span>
                    <input type="radio" name="star" value="v3" required><span>3</span>
                    <input type="radio" name="star" value="v2" required><span>2</span>
                    <input type="radio" name="star" value="v1" required><span>1</span>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="wFormRow">
            <div class="popupLabel">Отзыв* :</div>

            <div class="wFormInput">
                <textarea class="wTextarea" name="msg" required data-rule-minlength="10" data-msg-required="Введите текст" placeholder="Ваш отзыв"></textarea>
            </div>
        </div>

        <div class="tac">
            <button class="wSubmit wBtn lg">отправить отзыв</button>
        </div>
    </div>
</div>