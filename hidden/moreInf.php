
<div class="formPopup zoomAnim">
    <div class="h2T popupTitle">Заявка на уведомление</div>

    <div class="wForm wFormDef" data-form="true">

        <div class="wFormRow">
            <div class="profile-edit_label">Email:</div>

            <div class="wFormInput">
                <input type="email" class="wInput" name="email" required data-rule-email="true" data-msg-required="Укажите e-mail" >
            </div>
        </div>


        <div class="wFormRow">
            <div class="profile-edit_label">Введите пароль</div>

            <div class="wFormInput">
                <input type="password" name="enter_pass" data-msg-required="Это поле необходимо заполнить" data-msg-minlength="Пожалуйста, введите не меньше 4 символов" data-rule-minlength="4" placeholder="Пароль" required="">
            </div>

        </div>

        <div class="tac">
            <button class="wSubmit wBtn lg">отправить заявку</button>
        </div>
    </div>
</div>